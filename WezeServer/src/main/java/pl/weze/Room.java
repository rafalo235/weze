package pl.weze;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import pl.weze.protocol.Message;
import pl.weze.protocol.multicast.PositionData;

public class Room {
	//private List<Client> clients = new ArrayList<Client>(); // mo�e zmieni� na concurrent jak dojdzie wielow�tkowo��
	private Map<Client, Byte> clients = new HashMap<Client, Byte>();
	private byte availableId = 0;
	
	public synchronized void addClient(Client client) {
		if (!clients.containsKey(client))
			clients.put(client, availableId++);
	}
	
	public synchronized void sendPositionData(Client client, PositionData data, CommunicationChannel channel) {
		Iterator<Client> i = clients.keySet().iterator();
		byte id = clients.get(client);
		while (i.hasNext()) {
			Client c = i.next();
			if (!c.equals(client)) {
				data.setPlayerId(id);
				Message m = new Message();
				m.setClient(c);
				m.setDatagram(data);
				channel.send(m);
			}
		}
	}
}
