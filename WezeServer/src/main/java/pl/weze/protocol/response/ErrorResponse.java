package pl.weze.protocol.response;

import java.nio.ByteBuffer;

import pl.weze.protocol.GenericDatagram;
import pl.weze.protocol.PacketType;

public class ErrorResponse
		extends GenericDatagram {
	private byte code;
	
	public ErrorResponse() {
		setPacketType((byte)PacketType.ERROR.ordinal());
	}
	
	public ErrorResponse(byte code) {
		this();
		this.setCode(code);
	}
	
	public byte getCode() {
		return code;
	}

	public void setCode(byte code) {
		this.code = code;
	}
	
	public static ErrorResponse parse(ByteBuffer bytes) {
		ErrorResponse response = new ErrorResponse();
		response.setCode(bytes.get());
		return response;
	}

	@Override
	public int toBytes(ByteBuffer buffer) {
		buffer.put(getPacketType())
			.put(getCode());
		return 2;
	}

}
