package pl.weze.protocol.response;

import java.nio.ByteBuffer;

import pl.weze.protocol.GenericDatagram;
import pl.weze.protocol.PacketType;

public class JoinedRoomResponse
		extends GenericDatagram {
	protected int roomId;
	
	public JoinedRoomResponse() {
		setPacketType((byte)PacketType.JOINED_ROOM.ordinal());
	}
	
	public JoinedRoomResponse(int roomId) {
		this();
		this.roomId = roomId;
	}
	
	public int getRoomId() {
		return roomId;
	}
	public void setRoomId(int roomId) {
		this.roomId = roomId;
	}
	
	@Override
	public int toBytes(ByteBuffer buffer) {
		buffer.put(getPacketType())
			.putInt(getRoomId());
		return 5;
	}
	
	public static JoinedRoomResponse parse(ByteBuffer bytes) {
		JoinedRoomResponse data = new JoinedRoomResponse();
		data.setRoomId(bytes.getInt());
		return data;
	}
}
