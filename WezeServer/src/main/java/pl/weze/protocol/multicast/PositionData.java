package pl.weze.protocol.multicast;

import java.nio.ByteBuffer;

import pl.weze.protocol.GenericDatagram;
import pl.weze.protocol.PacketType;

public class PositionData
		extends GenericDatagram {
	private int roomId;
	private byte playerId;
	private float x;
	private float y;
	private byte state;
	private long order;
	
	public PositionData() {
		setPacketType((byte)PacketType.POSITION_DATA.ordinal());
	}
	
	public int getRoomId() {
		return roomId;
	}
	
	public void setRoomId(int roomId) {
		this.roomId = roomId;
	}
	
	public byte getPlayerId() {
		return playerId;
	}

	public void setPlayerId(byte playerId) {
		this.playerId = playerId;
	}

	public float getX() {
		return x;
	}

	public void setX(float x) {
		this.x = x;
	}

	public float getY() {
		return y;
	}

	public void setY(float y) {
		this.y = y;
	}
	
	public byte getState() {
		return state;
	}
	
	public void setState(byte state) {
		this.state = state;
	}

	public long getOrder() {
		return order;
	}

	public void setOrder(long order) {
		this.order = order;
	}
	
	public static PositionData parse(ByteBuffer bytes) {
		PositionData data = new PositionData();
		data.setRoomId(bytes.getInt());
		data.setPlayerId(bytes.get());
		data.setX(bytes.getFloat());
		data.setY(bytes.getFloat());
		data.setState(bytes.get());
		data.setOrder(bytes.getLong());
		return data;
	}

	@Override
	public int toBytes(ByteBuffer buffer) {
		buffer.put(getPacketType())
			.putInt(getRoomId())
			.put(getPlayerId())
			.putFloat(getX())
			.putFloat(getY())
			.put(getState())
			.putLong(getOrder());
		return 23;
	}

}
