package pl.weze.protocol;

import java.nio.ByteBuffer;

import pl.weze.protocol.multicast.PositionData;
import pl.weze.protocol.request.ConnectRequest;
import pl.weze.protocol.request.CreateRoomRequest;
import pl.weze.protocol.request.JoinRoomRequest;
import pl.weze.protocol.response.ErrorResponse;
import pl.weze.protocol.response.JoinedRoomResponse;

public abstract class GenericDatagram {
	protected byte packetType;
	protected byte crc; //???
	
	public byte getPacketType() {
		return packetType;
	}
	public void setPacketType(byte packetType) {
		this.packetType = packetType;
	}
	
	public abstract int toBytes(ByteBuffer buffer);
	
	public static GenericDatagram parse(ByteBuffer bytes) throws Exception {
		GenericDatagram datagram = null;
		byte type = bytes.get();
		if (type == PacketType.POSITION_DATA.ordinal()) {
			datagram = PositionData.parse(bytes);
		} else if (type == PacketType.CREATE_ROOM.ordinal()) {
			datagram = CreateRoomRequest.parse(bytes);
		} else if (type == PacketType.JOIN_ROOM.ordinal()) {
			datagram = JoinRoomRequest.parse(bytes);
		} else if (type == PacketType.CONNECT.ordinal()) {
			datagram = ConnectRequest.parse(bytes);
		} else if (type == PacketType.JOINED_ROOM.ordinal()) {
			datagram = JoinedRoomResponse.parse(bytes);
		} else if (type == PacketType.ERROR.ordinal()) {
			datagram = ErrorResponse.parse(bytes);
		} else {
			throw new Exception("Protocol fault");
		}
		datagram.setPacketType(type);
		return datagram;
	}
}
