package pl.weze.protocol;

public enum PacketType {
	CREATE_ROOM,
	JOIN_ROOM,
	CONNECT, // if there is a room - join; create_room otherwise
	JOINED_ROOM,
	ERROR,
	POSITION_DATA
}
