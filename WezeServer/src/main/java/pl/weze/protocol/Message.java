package pl.weze.protocol;

import pl.weze.Client;

public class Message {
	private Client client;
	private GenericDatagram datagram;
	
	public Client getClient() {
		return client;
	}
	public void setClient(Client client) {
		this.client = client;
	}
	public GenericDatagram getDatagram() {
		return datagram;
	}
	public void setDatagram(GenericDatagram datagram) {
		this.datagram = datagram;
	}
}
