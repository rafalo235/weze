package pl.weze.protocol.request;

import java.nio.ByteBuffer;

import pl.weze.protocol.GenericDatagram;
import pl.weze.protocol.PacketType;

public class JoinRoomRequest
		extends GenericDatagram {
	private int roomId;
	
	public JoinRoomRequest() {
		setPacketType((byte)PacketType.JOIN_ROOM.ordinal());
	}

	public int getRoomId() {
		return roomId;
	}
	public void setRoomId(int roomId) {
		this.roomId = roomId;
	}

	public static JoinRoomRequest parse(ByteBuffer bytes) {
		JoinRoomRequest request = new JoinRoomRequest();
		request.setRoomId(bytes.getInt());
		return request;
	}
	
	@Override
	public int toBytes(ByteBuffer buffer) {
		buffer.put(getPacketType())
			.putInt(getRoomId());
		return 1;
	}
}
