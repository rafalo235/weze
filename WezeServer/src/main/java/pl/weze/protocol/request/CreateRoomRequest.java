package pl.weze.protocol.request;

import java.nio.ByteBuffer;

import pl.weze.protocol.GenericDatagram;
import pl.weze.protocol.PacketType;

public class CreateRoomRequest
		extends GenericDatagram {
	public CreateRoomRequest() {
		setPacketType((byte)PacketType.CREATE_ROOM.ordinal());
	}
	
	public static CreateRoomRequest parse(ByteBuffer bytes) {
		CreateRoomRequest request = new CreateRoomRequest();
		return request;
	}
	
	@Override
	public int toBytes(ByteBuffer buffer) {
		buffer.put(getPacketType());
		return 1;
	}
}
