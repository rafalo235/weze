package pl.weze.protocol.request;

import java.nio.ByteBuffer;

import pl.weze.protocol.GenericDatagram;
import pl.weze.protocol.PacketType;

public class ConnectRequest extends GenericDatagram {

	public ConnectRequest() {
		setPacketType((byte)PacketType.CONNECT.ordinal());
	}
	
	public static ConnectRequest parse(ByteBuffer bytes) {
		ConnectRequest request = new ConnectRequest();
		return request;
	}
	
	@Override
	public int toBytes(ByteBuffer buffer) {
		buffer.put(getPacketType());
		return 1;
	}

}
