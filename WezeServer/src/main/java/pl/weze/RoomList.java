package pl.weze;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import pl.weze.exception.NoRoomException;
import pl.weze.protocol.multicast.PositionData;

public class RoomList {
	private Map<Integer, Room> rooms = new ConcurrentHashMap<Integer, Room>();
	private Map<Client, Integer> connected = new ConcurrentHashMap<Client, Integer>();
	private int availableId = Integer.MIN_VALUE;

	public synchronized int addRoom(Client client) {
		int assignedId;
		Integer wrapper;
		
		// Client already created room
		if ((wrapper = connected.get(client)) != null)
			return wrapper;
		
		while (true) {
			// Search unused key
			if (!rooms.containsKey(availableId)) {
				Room room = new Room();
				rooms.put(availableId, room);
				room.addClient(client);
				connected.put(client, availableId);
				assignedId = availableId++;
				break;
			} else {
				availableId++;
			}
		}
		return assignedId;
	}
	
	public synchronized void removeRoom(Client client) {
		rooms.remove(connected.get(client));
		connected.remove(client);
	}
	
	public synchronized int joinRoom(Client client, int roomId) throws NoRoomException {
		Integer wrapper;
		if ((wrapper = connected.get(client)) != null)
			return wrapper;
		Room room = rooms.get(roomId);
		if (room == null)
			throw new NoRoomException();
		room.addClient(client);
		connected.put(client, roomId);
		return roomId;
	}
	
	public synchronized int connect(Client client) {
		Integer wrapper;
		if ((wrapper = connected.get(client)) != null)
			return wrapper;
		if (rooms.isEmpty())
			return addRoom(client);
		int roomId = rooms.keySet().iterator().next();
		rooms.get(roomId).addClient(client);
		connected.put(client, roomId);
		return roomId;
	}
	
	public void sendPositionData(Client client, PositionData data, CommunicationChannel channel) {
		rooms.get(data.getRoomId()).sendPositionData(client, data, channel);
	}
	
	public Room get(int id) {
		return rooms.get(id);
	}
	
}
