package pl.weze;

import pl.weze.exception.NoRoomException;
import pl.weze.protocol.Message;
import pl.weze.protocol.multicast.PositionData;
import pl.weze.protocol.request.ConnectRequest;
import pl.weze.protocol.request.CreateRoomRequest;
import pl.weze.protocol.request.JoinRoomRequest;
import pl.weze.protocol.response.ErrorCode;
import pl.weze.protocol.response.ErrorResponse;
import pl.weze.protocol.response.JoinedRoomResponse;

public class Controller {
	private RoomList roomList = new RoomList();
	private CommunicationChannel channel;
	
	public Controller(CommunicationChannel channel) {
		this.channel = channel;
	}
	
	public void dispatch(Message message) {
		if (message.getDatagram() instanceof PositionData) {
			PositionData data = (PositionData)message.getDatagram();
			roomList.sendPositionData(message.getClient(), data, channel);
		} else if (message.getDatagram() instanceof ConnectRequest) {
			int id = roomList.connect(message.getClient());
			Message response = new Message();
			response.setClient(message.getClient());
			response.setDatagram(new JoinedRoomResponse(id));
			channel.send(response);
		} else if (message.getDatagram() instanceof JoinRoomRequest) {
			try {
				int id = roomList.joinRoom(message.getClient(), ((JoinRoomRequest)message.getDatagram()).getRoomId());
				Message response = new Message();
				response.setClient(message.getClient());
				response.setDatagram(new JoinedRoomResponse(id));
				channel.send(response);
			} catch(NoRoomException e) {
				Message error = new Message();
				error.setClient(message.getClient());
				error.setDatagram(new ErrorResponse((byte)ErrorCode.NO_SUCH_ROOM.ordinal()));
				channel.send(message);
			}
		} else if (message.getDatagram() instanceof CreateRoomRequest) {
			int id = roomList.addRoom(message.getClient());
			Message response = new Message();
			response.setClient(message.getClient());
			response.setDatagram(new JoinedRoomResponse(id));
			channel.send(response);
		}
	}

}
