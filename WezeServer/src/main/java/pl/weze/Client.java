package pl.weze;

import java.net.InetSocketAddress;

public class Client {
	private InetSocketAddress address;
	
	public Client(InetSocketAddress address) {
		this.address = address;
	}
	public InetSocketAddress getAddress() {
		return address;
	}
	public void setAddress(InetSocketAddress address) {
		this.address = address;
	}
	
	@Override
	public int hashCode() {
		return address.toString().hashCode();
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (obj == null || getClass() != obj.getClass()) return false; 
		return ((Client)obj).address.equals(address);
	}
}
