package pl.weze;

import java.io.IOException;

import pl.weze.protocol.Message;

public class ServerApp 
{
	public static final int SERVER_PORT = 8008;
	
    public static void main( String[] args )
    		throws IOException {
    	CommunicationChannel channel = new CommunicationChannel(SERVER_PORT);
    	Controller controller = new Controller(channel);
    	
		while (true) {
			Message m = channel.receive();
			if (m != null)
				controller.dispatch(m);
		}
    }
}
