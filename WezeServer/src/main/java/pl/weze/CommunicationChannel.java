package pl.weze;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.DatagramChannel;
import java.util.Queue;
import java.util.concurrent.LinkedBlockingQueue;

import pl.weze.protocol.GenericDatagram;
import pl.weze.protocol.Message;

public class CommunicationChannel {
	private DatagramChannel socket;
	private Queue<Message> out = new LinkedBlockingQueue<Message>();
	private Queue<Message> in = new LinkedBlockingQueue<Message>();
	private Thread listener, sender;
	private final int MAX_MESSAGE_LENGTH = 23;
	
	public CommunicationChannel()
			throws IOException {
		socket = DatagramChannel.open();
		socket.configureBlocking(true);
		socket.bind(new InetSocketAddress((int)(Math.random() * 50) + 10000));
		listener = new Thread(new ListenerThread());
		sender = new Thread(new SenderThread());
		listener.start();
		sender.start();
	}
	
	public CommunicationChannel(int port)
			throws IOException {
		socket = DatagramChannel.open();
		socket.configureBlocking(true);
		socket.bind(new InetSocketAddress(port));
		listener = new Thread(new ListenerThread());
		sender = new Thread(new SenderThread());
		listener.start();
		sender.start();
	}

	public void send(Message message) {
		out.add(message);
	}
	
	public Message receive() {
		return in.poll();
	}
	
	class ListenerThread implements Runnable {
		
		public void run() {
			ByteBuffer wrapper = ByteBuffer.wrap(new byte[MAX_MESSAGE_LENGTH]);
			
			while(true) {
				try {
					wrapper.clear();
					SocketAddress addr = socket.receive(wrapper);
					
					wrapper.position(0);
					Message message = new Message();
					message.setClient(new Client((InetSocketAddress)addr));
					try {message.setDatagram(GenericDatagram.parse(wrapper)); } catch (Exception e) {System.err.println(e.getMessage()); continue; }
					
					in.add(message);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		
	}
	
	class SenderThread implements Runnable {
		
		public void run() {
			Message m = null;
			ByteBuffer buffer = ByteBuffer.wrap(new byte[MAX_MESSAGE_LENGTH]);
			while(true) {
				if ((m = out.poll()) != null) {
					buffer.clear();
					m.getDatagram().toBytes(buffer);
					buffer.flip();
					try {
						socket.send(buffer, m.getClient().getAddress());
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
		}
		
	}

}
