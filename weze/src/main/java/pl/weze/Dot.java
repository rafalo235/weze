package pl.weze;

import processing.core.PApplet;

public class Dot {
	private float x;
	private float y;
	private float prevX;
	private float prevY;
	private byte state = 0;
	
	public Dot(float x, float y) {
		this.x = x;
		this.y = y;
		this.prevX = x;
		this.prevY = y;
	}
	
	public float getX() {
		return x;
	}
	
	public float getY() {
		return y;
	}
	
	public void setPosition(float x, float y) {
		this.prevX = this.x;
		this.prevY = this.y;
		this.x = x;
		this.y = y;
	}
	
	public byte getState() {
		return state;
	}
	
	public void setState(byte state) {
		this.state = state;
	}
	
	public void draw(PApplet app) {
		if (state == 1)
			return;
		app.stroke(255f);
		app.strokeWeight(2.0f);
		app.line(prevX, prevY, x, y);
	}
	
}
