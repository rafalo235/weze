package pl.weze;

public class DotController {
	private Dot dot;
	private double angle;
	private final float speed = 7.f;
	private byte state = 0;
	
	public DotController(Dot dot) {
		this.dot = dot;
		this.angle = Math.random() * 360.0;
	}
	
	public void onSteer(int keyCode) {
		if (keyCode == processing.core.PConstants.LEFT)
			angle += 0.5;
		else if (keyCode == processing.core.PConstants.RIGHT)
			angle -= 0.5;
	}
	
	public void update() {
		dot.setState(state);
		dot.setPosition(
				(float) (dot.getX() + speed * Math.sin(angle)),
				(float) (dot.getY() + speed * Math.cos(angle)));
	}
	
}
