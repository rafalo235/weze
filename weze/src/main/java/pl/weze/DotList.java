package pl.weze;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import processing.core.PApplet;

public class DotList {
	private Map<Byte, Dot> others = new HashMap<Byte, Dot>();
	
	public synchronized void draw(PApplet app) {
		Iterator<Dot> i = others.values().iterator();
		while(i.hasNext()) {
			i.next().draw(app);
		}
	}
	
	public synchronized void setPostion(byte id, float x, float y, byte state) {
		Dot d = others.get(id);
		if (d != null) {
			d.setPosition(x, y);
			d.setState(state);
		} else
			others.put(id, new Dot(x, y));
	}

}
