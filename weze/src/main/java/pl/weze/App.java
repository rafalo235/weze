package pl.weze;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.ArrayList;

import pl.weze.protocol.Message;
import pl.weze.protocol.multicast.PositionData;
import pl.weze.protocol.request.ConnectRequest;
import pl.weze.protocol.response.JoinedRoomResponse;
import processing.core.PApplet;

/**
 * Hello world!
 *
 */
public class App extends PApplet {
	private CommunicationChannel channel;
	private String serverAddress = "127.0.0.1";
	private int roomId = 0;
	private Client serverSide;
	
	private Thread controller;
	
	private boolean started = false;
	
	// --------
	private Dot player;
	private DotList others;
	
	private DotController dotController;
	// --------
	
	
	/*public static boolean IS_GAME_RUNNING = false;

	public static final int STATE_CONNECT = 1;
	public static final int STATE_BEFORE_PLAY = 2;
	public static final int STATE_PLAY = 3;
	public static final int STATE_FINNISH_PART = 4;
	public static final int STATE_FINNISH_GAME = 5;

	public int state_active = App.STATE_PLAY;

	public int counter_elapsed_millis = 0;
	public int counter_play_time = 0;

	ArrayList<Player> players;
	Player p;
	Player p2;*/

	public void setup() {
		int x = 800;
		int y = 600;
		frameRate(10);
		background(color(0));
		size(x, y,JAVA2D);
		
		serverSide = new Client(new InetSocketAddress(serverAddress, ServerApp.SERVER_PORT));
		
		try {
			channel = new CommunicationChannel();
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(1);
		}

		
		player = new Dot((float)(Math.random() * x), (float)(Math.random() * y));
		dotController = new DotController(player);
		others = new DotList();
		controller = new Thread(new ClientController(this, channel, others));
		
		Message m = new Message(),
				back = null;
		m.setClient(serverSide);
		m.setDatagram(new ConnectRequest());
		while (true) {
			channel.send(m);
			try {
				Thread.sleep(100L);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			if ((back = channel.receive()) != null) {
				roomId = ((JoinedRoomResponse)back.getDatagram()).getRoomId();
				break;
			}
		}

		//players = new ArrayList<Player>();
		
		/*p = new Player( this, (int) (Math.random() * x), (int) (Math.random() * y) , (int)(Math.random() * 360 ), this.color(254,250,124) );
		p2 = new Player( this, (int) (Math.random() * x), (int) (Math.random() * y) , (int)(Math.random() * 360 ), this.color(125,255,255) );
		players.add(p);
		players.add(p2);*/
		
		
		controller.start();

		smooth(8);
	}

	public void draw() {
		player.draw(this);
		others.draw(this);
		
		Message m = new Message();
		PositionData data = new PositionData();
		data.setRoomId(roomId);
		data.setX(player.getX());
		data.setY(player.getY());
		data.setState(player.getState());
		m.setClient(serverSide);
		m.setDatagram(data);
		channel.send(m);
		
		if (started == false)
			return;

		if ( keyPressed && key == CODED) {
			dotController.onSteer(keyCode);
		}
		dotController.update();
		loadPixels();
		if (this.get((int)player.getX(), (int)player.getY()) != color(0))
			System.exit(0);
	}
	
	public boolean isStarted() {
		return started;
	}
	
	public void setStarted(boolean started) {
		this.started = started;
	}
}