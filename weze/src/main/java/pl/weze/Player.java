package pl.weze;

import processing.core.PApplet;
import processing.core.PGraphics;

public class Player {

	PApplet p;
	
	int size = 12;
	int hsize = 6;
	
	float speed_counter = 1.5f;
	
	float angle_multipler = 2.0f; 
	
	private float posX = 0, posY = 0;
	private float oldPosX = 0, oldPosY = 0; 
	
	int color;
	
	boolean alive = true;
	boolean hole_mode = true;
	
	private float counter_hole_length = 0.0f;
	private static final float length_hole_mode = 4.0f;
		
	float angle = 0.0f;
	
	public PGraphics path;
	
	public Player( PApplet parent, int x, int y, int angle, int color ){
		
		this.p = parent;
		
		this.posX = x;
		this.posY = y;
		this.angle = angle;
		this.color = color;
		
		this.path = this.p.createGraphics(p.width,p.height,PApplet.JAVA2D);
		this.path.beginDraw();
		this.path.endDraw();
	}
	
	public synchronized void setPosition(float posX, float posY) {
		oldPosX = this.posX;
		oldPosY = this.posY;
		this.posX = posX;
		this.posY = posY;
	}
	
	public synchronized float getX() {
		return posX;
	}
	
	public synchronized float getY() {
		return posY;
	}
	
	public synchronized float getOldX() {
		return oldPosX;
	}
	
	public synchronized float getOldY() {
		return oldPosY;
	}
	
	public void setUpAngle(){
		this.angle = (this.angle + this.angle_multipler) % 360;		
	}
	
	public void setDownAngle() {
		this.angle -= this.angle_multipler;
		if( this.angle < 0.0f ){
			this.angle = 360.0f - this.angle; 
		}
	}
	
	public void sizeUp(){
		this.size *= 2;
		this.hsize *= 2;
	}
	
	public void sizeDown(){
		this.size *= 0.5f;
		this.hsize *= 0.5f;
	}
	
	public void draw( int elapsed_millis ){
		
		p.image(this.path, 0, 0);
		
		if( this.alive ){
		
			p.stroke( this.color );
			p.strokeWeight(this.size);
			
			float ang = this.angle * PApplet.PI * 0.005555555f;
			
			float speedmul = this.speed_counter * (float)elapsed_millis * 0.1f;
			
			float diffx = PApplet.cos( ang ) * speedmul;
			float diffy = PApplet.sin( ang ) * speedmul;
			
			float nx = this.getX() + diffx;   
			float ny = this.getY() + diffy;
			this.setPosition(nx, ny);
			
			if( this.hole_mode && this.counter_hole_length < ( this.size * Player.length_hole_mode ) ){
				this.counter_hole_length += speedmul;
			} else {
				if ( this.hole_mode ){
					this.hole_mode = false;	
					this.counter_hole_length = 0.0f;
				}
				this.path.beginDraw();
				this.path.smooth(8);
				this.path.stroke( this.color );
				this.path.strokeWeight(this.size);
				this.path.line(getOldX(), getOldY(), getX(), getY());
				this.path.endDraw();
			} 
												
			if ( p.random(1.0f, 100.0f) < 2.0f ){
				this.hole_mode = true;
			} 
			
			this.p.loadPixels();
			if( p.get( (int)(this.posX + diffx * (this.hsize+2)), (int)(this.posY + diffy * (this.hsize+2) ) ) != p.color(0) ){
				//this.alive = false;
			}
				
			p.point(nx, ny);
							
			this.posX = nx;
			this.posY = ny;
		}
		
	}
	
}
