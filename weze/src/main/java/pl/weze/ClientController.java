package pl.weze;

import pl.weze.protocol.Message;
import pl.weze.protocol.multicast.PositionData;

public class ClientController
		implements Runnable {
	private App app;
	private CommunicationChannel channel;
	private DotList others;
	
	public ClientController(App app, CommunicationChannel channel, DotList others) {
		this.app = app;
		this.channel = channel;
		this.others = others;
	}
	
	@Override
	public void run() {
		
		while(true) {
			Message m;
			if ((m = channel.receive()) != null) {
				if (m.getDatagram() instanceof PositionData) {
					PositionData data = (PositionData)m.getDatagram();
					others.setPostion(data.getPlayerId(), data.getX(), data.getY(), data.getState());
					if (app.isStarted())
						continue;
					app.setStarted(true);
				}
			}
		}
		
	}

}
